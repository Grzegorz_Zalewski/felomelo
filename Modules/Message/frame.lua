function createFrames()
    --create killing blow message frame.
    message = CreateFrame("MessageFrame", "KBFrameM", UIParent)

    --set place to display message in the center
    message:SetPoint(Felomelo:GetKBIPoint(),Felomelo:GetKBMRelTo(),Felomelo:GetKBMRelPoint(),Felomelo:GetKBIXOffset(),Felomelo:GetKBIYOffset())

    --set layer at high(ATTENTION CHILD FRAMES ALWAYS GET INSERTED ABOVE THEIR PARENT)
    message:SetFrameStrata("HIGH")

    --set height of message
    message:SetSize(Felomelo:GetKBMWidth(info),Felomelo:GetKBMHeight(info))

    --set amount of seconds before message fades(3 seconds)
    message:SetTimeVisible(Felomelo:GetVisibleTime(info))

    --set amount of seconds it takes for message to fade(1 second)
    message:SetFadeDuration(Felomelo:GetFadeTime(info))

    --set message font,font-size,and flags
    --max font size 30
    --flags:
    --MONOCHROME - Font is rendered without antialiasing
    --OUTLINE - Font is displayed with a black outline
    --THICKOUTLINE - Font is displayed with a thick black outline
    message:SetFont(Media:Fetch("font", Felomelo:GetFont(info)), Felomelo:GetFontSize(info), Felomelo:GetOutline(info))

    image = CreateFrame("Frame", "KBFrameI", UIParent)

    --set image at the center
    image:SetPoint(Felomelo:GetKBIPoint(),Felomelo:GetKBIRelTo(),Felomelo:GetKBIRelPoint(),Felomelo:GetKBIXOffset(),Felomelo:GetKBIYOffset())

    --set frame on high level
    image:SetFrameStrata("HIGH")
    image:SetSize(Felomelo:GetKBIWidth(info),Felomelo:GetKBIHeight(info))

    --create image texture
    imageObj = image:CreateTexture()
    imageObj:SetAllPoints()
    imageObj:SetAlpha(-1)
    imageObj:SetTexture(Felomelo:GetKBIImage(info))

    --hide image intially
    image:Hide()

    --create animation groups
    imageAnimationIn = imageObj:CreateAnimationGroup()
    imageAnimationOut = imageObj:CreateAnimationGroup()

    --crette alphaIn and Out
    alphaIn = imageAnimationIn:CreateAnimation("Alpha")
    alphaOut = imageAnimationOut:CreateAnimation("Alpha")

    lockImageFrame()
    lockMessageFrame()

    if(Felomelo:GetKBIDebug(info) == true) then
            fade();
    end
end