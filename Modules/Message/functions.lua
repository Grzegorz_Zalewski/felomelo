--setFont when changed in options menu
function setFont()
    message:SetFont(Media:Fetch("font", Felomelo:GetFont(info)), Felomelo:GetFontSize(info), Felomelo:GetOutline(info))
end

--setColors when changed in options menu
function setColors()
    if (Felomelo:EnableSnap(info) == true) then
        message:Clear()
        message:AddMessage(Felomelo:GetKillingMessage(info),Felomelo:GetRed(info),Felomelo:GetGreen(info),Felomelo:GetBlue(info))
    end
end

--setTime when changed in options menu
function setTime()
    if (Felomelo:EnableSnap(info) == false) then
        message:SetTimeVisible(Felomelo:GetVisibleTime(info))
        message:SetFadeDuration(Felomelo:GetFadeTime(info))
    end
end

--setKillingMsg when changed in options menu
function setKillingMsg()
    if (Felomelo:EnableSnap(info) == true) then
        message:Clear()
        message:AddMessage(Felomelo:GetKillingMessage(info),Felomelo:GetRed(info),Felomelo:GetGreen(info),Felomelo:GetBlue(info))
    end
end

--Snap killing blow message frame to window when enabled and when disabled clear and set timevisible back to normal
function snapMessage()
    --EnableSnap is false then set back to database value else if edit mode is true then set time to 10000
    if (Felomelo:EnableSnap(info) == false) then
        message:SetTimeVisible(Felomelo:GetVisibleTime(info))
        message:Clear()
    else
        message:SetTimeVisible(10000)
        message:AddMessage(Felomelo:GetKillingMessage(info),Felomelo:GetRed(info),Felomelo:GetGreen(info),Felomelo:GetBlue(info))
    end

end

--Method for fading the killing blow message in and out
function fade()
    --set duration it takes to finish the fade in and fade out
    i = Felomelo:GetFadeInTime(info)
    if(i == 0) then
        i = 0.1
    end

    o = Felomelo:GetFadeOutTime(info)
    if(o == 0) then
        o = 0.1
    end

    alphaIn:SetDuration(i)
    alphaOut:SetDuration(o)

    --set delay for waiting when fadein ends
    alphaIn:SetEndDelay(Felomelo:GetEndDelay(info))

    imageAnimationIn:SetScript("OnPlay", function(self)
        image:Show();
    end)

    alphaIn:SetScript("OnPlay", function(self)
        alphaIn:SetFromAlpha(0)
        alphaIn:SetToAlpha(1)
    end)

    alphaOut:SetScript("OnPlay", function(self)
        alphaOut:SetFromAlpha(1)
        alphaOut:SetToAlpha(0)
    end)

    imageAnimationIn:SetScript("OnFinished", function(self)
        imageObj:SetAlpha(1)
        imageAnimationOut:Play();
    end)

    imageAnimationOut:SetScript("OnFinished", function(self)
        imageObj:SetAlpha(-1)
        image:Hide();

        if(Felomelo:GetKBIDebug(info) == true) then
            fade();
        end
    end)

    imageAnimationIn:Play();
end

function resizeKBIImage(w,h)
    image:SetSize(w,h)
end

function resizeKBMMessage(w,h)
    message:SetSize(w,h)
end

--lock or unlock kbtracker based on options
function lockMessageFrame()
    if (Felomelo:EnableMessageDrag(info) == true) then
        message:SetMovable(true)
        message:EnableMouse(true)
        message:RegisterForDrag("LeftButton")

        message:SetScript("OnDragStart", message.StartMoving)
        message:SetScript("OnDragStop", message.StopMovingOrSizing)
        message:SetScript('OnLeave', function() 
            point, relativeTo, relativePoint, xOffset, yOffset = message:GetPoint();

            Felomelo:SetKBMPoint(nil, point);
            Felomelo:SetKBMRelTo(nil, relativeTo);
            Felomelo:SetKBMRelPoint(nil, relativePoint);
            Felomelo:SetKBMXOffset(nil, xOffset);
            Felomelo:SetKBMYOffset(nil, yOffset);
         end)
    else
        message:SetMovable(false)
        message:EnableMouse(false)
        message:RegisterForDrag(nil)

        message:SetScript("OnDragStart", nil)
        message:SetScript("OnDragStop", nil)
    end
end

--lock or unlock kbtracker based on options
function lockImageFrame()
    if (Felomelo:EnableImageDrag(info) == true) then
        image:SetMovable(true)
        image:EnableMouse(true)
        image:RegisterForDrag("LeftButton")

        image:SetScript("OnDragStart", image.StartMoving)
        image:SetScript("OnDragStop", image.StopMovingOrSizing)
        image:SetScript('OnLeave', function() 
            point, relativeTo, relativePoint, xOffset, yOffset = image:GetPoint();

            Felomelo:SetKBIPoint(nil, point);
            Felomelo:SetKBIRelTo(nil, relativeTo);
            Felomelo:SetKBIRelPoint(nil, relativePoint);
            Felomelo:SetKBIXOffset(nil, xOffset);
            Felomelo:SetKBIYOffset(nil, yOffset);
         end)
    else
        image:SetMovable(false)
        image:EnableMouse(false)
        image:RegisterForDrag(nil)

        image:SetScript("OnDragStart", nil)
        image:SetScript("OnDragStop", nil)
    end
end

function resetSoundsOnDeath()
    if (Felomelo:EnableSoundOrderedRestartDeath() == true) then
        local startUp = CreateFrame("Frame")
        startUp:RegisterEvent("PLAYER_DEAD");
        startUp:SetScript("OnEvent", function(self, event, ...)
            Felomelo:ResetSoundsOrderedOrder()
        end);
    end
end

function resetSoundsOnZoneChange()
    if (Felomelo:EnableSoundOrderedRestartZone() == true) then
        local startUp = CreateFrame("Frame")
        startUp:RegisterEvent("ZONE_CHANGED_NEW_AREA");
        startUp:SetScript("OnEvent", function(self, event, ...)
            Felomelo:ResetSoundsOrderedOrder()
        end);
    end
end

function showSoundsTimerVal()
    local options = options.args.sound.args.sound_ordered.args
    if (Felomelo:EnableSoundOrderedRestartTime() == true) then
        options['time'] = {
            name = 'Reset time',
            order = 4,
            type = "select",
            values = sounds_ordered_time_options,
            get = function(info) return Felomelo:GetSoundsOrderedTimeVal(info) end,
            set = function(info, key) Felomelo:SetSoundsOrderedTimeVal(info, key) end,
        }
    else
        options['time'] = nil;
    end
end

function showSoundsOrderedList()
    local sounds_ordered = Felomelo:GetSoundOrderedIndex()
    if (sounds_ordered ~= 1) then
        for i=1, (sounds_ordered - 1) do
            local options = options.args.sound.args.sound_ordered.args
            local index = tostring(i)
            local label = index .. "."

            options[index] = {
                name = label,
                order = index + 6,
                width = "full",
                type = "select",
                dialogControl = 'LSM30_Sound',
                values = Media:HashTable("sound"),
                get = function(info) return Felomelo:GetSoundOrderedListItem(info) end,
                set = function(info, key) Felomelo:SetSoundOrderedListItem(info, key) end,
            }
        end
    end
end

local soundsTimer = CreateFrame("frame")
function resetSoundsOnTimer(index)
    local total = 0
    local index = index
    local function onUpdate(self,elapsed)
        total = total + elapsed
        if total >= Felomelo:GetSoundsOrderedTimeVal()  then
            soundsTimer:SetScript("OnUpdate", nil);
            if index == Felomelo:GetSoundsOrderedOrder() and  Felomelo:EnableSoundOrderedRestartTime() == true then
                Felomelo:ResetSoundsOrderedOrder();
            end
        end
    end

    soundsTimer:SetScript("OnUpdate", onUpdate);
end