damageEvent = {
    ["SWING_DAMAGE"] = true,
    ["RANGE_DAMAGE"] = true,
    ["SPELL_DAMAGE"] = true,
    ["SPELL_PERIODIC_DAMAGE"] = true,
}

checkKill = CreateFrame("Frame")
checkKill:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
checkKill:SetScript("OnEvent", function(self, event, timestamp, subevent, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags, ...)
    local spellID, spellName, spellSchool, amount, overkill
    local playerName = UnitName("player");
    local petName = UnitName("pet");
    local _, overkill
    --print(subevent+"");
    if ((subevent == "Phoenix's Flames")
            and (destFlags == 66888  or Felomelo:EnablePVP(info) == false))

    then
        if(Felomelo:EnableMSGAnnounce(info) == true)then
            --|c
            local className, classId, raceName, raceId, gender, name, realm
            local classNamePlayer, classId, raceNamePlayer, raceId, genderPlayer, namePlayer, realmPlayer = GetPlayerInfoByGUID(UnitGUID("player"))
            SetMapToCurrentZone()
            local mapName = GetMapNameByID(GetCurrentMapAreaID())
            local gender = genderValues[gender]
            local genderPlayer = genderValues[genderPlayer]
            local message = Felomelo:GetAnnounceMessage(info)
            local level = UnitLevel("player")

            if (raceName ~= nil) then
                local raceName = raceName:lower();
            end

            if (destFlags ~= 66888) then
                message = Felomelo:GetAnnounceMessageNon(info)
                name = destName;
            end

            if(Felomelo:GetChatAnnounceChannel(info) == "private") then
                local playerName = "|cff" .. classColors[classNamePlayer] .. playerName .. "|r";
                local classNamePlayer = "|cff" .. classColors[classNamePlayer] .. classNamePlayer:lower() .. "|r";

                if (destFlags == 66888) then
                    name = "|cff" .. classColors[className] .. name .. "|r";
                    className = "|cff" .. classColors[className] .. className:lower() .. "|r";
                end

                print(interp(message,{player = playerName,enemy = name,class = className,zone = mapName, gender = gender, race = raceName, realm = realm, playerlevel = level, playerclass = classNamePlayer, playerrace = raceNamePlayer, playergender = genderPlayer, playerrealm = realmPlayer}))
            else
                if (className ~= nil) then
                    local className = className:lower();
                end
                local classNamePlayer = classNamePlayer:lower();

                SendChatMessage((interp(message,{player = playerName,enemy = name,class = className,zone = mapName, gender = gender, race = raceName, realm = realm, playerlevel = level, playerclass = classNamePlayer, playerrace = raceNamePlayer, playergender = genderPlayer, playerrealm = realmPlayer})),Felomelo:GetChatAnnounceChannel(info))
            end

        end

        --IF enableKB = true
        if (Felomelo:EnableKB(info) == true)
            --THEN addMessage
        then
            if(Felomelo:GetModus(info) == 1) then
                message:AddMessage(Felomelo:GetKillingMessage(info),Felomelo:GetRed(info),Felomelo:GetGreen(info),Felomelo:GetBlue(info))
            elseif(Felomelo:GetModus(info) == 2) then
                imageAnimationIn:Stop();
                imageAnimationOut:Stop();
                fade();
            end
        end

        if (Felomelo:EnableEmotes(info) == true)
            --THEN do random emote
        then
            emote = Felomelo:GetRandomEmote();
            if emote ~= nil then
                 DoEmote(Felomelo:GetRandomEmote());
            end
        end
       

        --IF enableKBSound = true
        if (Felomelo:EnableKBSound(info) == true)
            --THEN playsoundfile
        then
            if(Felomelo:GetSoundModus(info) == 1) then
                local index = Felomelo:GetSoundsOrderedOrder(info)
                local sound = Felomelo:GetSoundOrderedListItem(info, index)
                if sound ~= nil then
                    PlaySoundFile(Media:Fetch("sound", sound, Felomelo:GetSoundLevel(info)));
                    Felomelo:IncrementSoundsOrderedOrder();

                    if (Felomelo:EnableSoundOrderedRestartTime()) then
                        resetSoundsOnTimer(Felomelo:GetSoundsOrderedOrder(info));
                    end
                end
            elseif(Felomelo:GetSoundModus(info) == 2) then
                local sound = Felomelo:GetRandomSound();
                if sound ~= nil then
                    PlaySoundFile(Media:Fetch("sound", sound, Felomelo:GetSoundLevel(info)));
                end
            end
        end

        --IF enableKBT = true
        if (Felomelo:EnableKBT(info) == true)
            --THEN update kbtracker
        then
            updateKbtracker();
        end

        --IF chat message is enabled(variable enableMSG needs to be true for this)
        if (Felomelo:EnableMSG(info) == true)
            --THEN get message from variable message and send it to the chat chat
        then
            if(Felomelo:GetChatChannel(info) == "private") then
                print(Felomelo:GetMessage(info))
            else
                SendChatMessage(Felomelo:GetMessage(info),Felomelo:GetChatChannel(info));
            end
        end
    end
end);