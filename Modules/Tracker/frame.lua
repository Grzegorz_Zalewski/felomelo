--set kbs_tracker variables to zero
kbs_session = 0;
kbs_cZone = 0;
kbs_pZone = 0;

-- Creating kbtracker frame
kbtracker = CreateFrame("Frame", "DragFrame2", UIParent)

--set frame to move on left mouse button
kbtracker:SetMovable(true)
kbtracker:EnableMouse(true)
kbtracker:RegisterForDrag("LeftButton")

--set default place for frame
kbtracker:SetPoint("CENTER")
--95
kbtracker:SetSize(200,90)

--create textframe within kbtracker
kbtrackerText =  kbtracker:CreateFontString("frametext" ,"ARTWORK","GameFontNormal");
kbtrackerTextVars =  kbtracker:CreateFontString("frametext" ,"ARTWORK","GameFontNormal");
kbtrackerTextProc =  kbtracker:CreateFontString("frametext" ,"ARTWORK","GameFontNormal");

--set position of text in frame
kbtrackerText:SetPoint("TopLeft", kbtracker, "TopLeft", 5, -8)
kbtrackerTextVars:SetPoint("TopLeft", kbtracker, "TopLeft", 130, -22)
kbtrackerTextProc:SetPoint("TopLeft", kbtracker, "TopLeft", 160, -16)


--align text
kbtrackerText:SetJustifyH("left")
kbtrackerTextVars:SetJustifyH("right")
kbtrackerTextProc:SetJustifyH("right")