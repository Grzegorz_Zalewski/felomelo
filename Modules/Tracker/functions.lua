--create a frame and register when player changes zones
--EXAMPLE from jade forest -> Kun lai = event occured
--EXAMPLE from out of bg -> in to bg = event occured
--EXAMPLE from in of bg -> out of bg = event occured
resetTrackerVariable = CreateFrame("Frame")
resetTrackerVariable:RegisterEvent("ZONE_CHANGED_NEW_AREA")

playerLeave = CreateFrame("Frame")
playerLeave:RegisterEvent("PLAYER_LOGOUT")
playerLeave:SetScript("OnEvent",function(self, event, addon)
    Felomelo:SetPrevSession(info, kbs_session)
end)

--on end of bg set kbs_bg variable to zero and update kbtracker text.
resetTrackerVariable:SetScript("OnEvent",function(self, event, addon)
    kbs_pZone = kbs_cZone;
    kbs_cZone = 0;
    updateValuesTracker()
end)

function setTrackerFont()
    kbtrackerText:SetFont(Media:Fetch("font", Felomelo:GetTrackerFont(info)), 12, "OUTLINE")
    kbtrackerTextVars:SetFont(Media:Fetch("font", Felomelo:GetTrackerFont(info)), 12, "OUTLINE")
    kbtrackerTextProc:SetFont(Media:Fetch("font", Felomelo:GetTrackerFont(info)), 12, "OUTLINE")
    width = kbtrackerText:GetWidth() + kbtrackerTextVars:GetWidth() + kbtrackerTextProc:GetWidth()
    kbtracker:SetWidth(math.floor(width))
end

function setBackdrop()
    --setbackground and border for tracker
    backdrop = {
        bgFile = Media:Fetch("background", Felomelo:GetTrackerBackground(info)),
        edgeFile= Media:Fetch("border", Felomelo:GetTrackerBorder(info)),
        tile = false,
        tileSize = 32,
        edgeSize = 10,
        insets = {
            left = 0,
            right = 0,
            top = 0,
            bottom = 0
        }
    }
    kbtracker:SetBackdrop(backdrop);
end

function updateValuesTracker()
    if(kbs_pZone == 0) then
        new = kbs_cZone + 1;
        old = kbs_pZone + 1;
        p = format("%.2f",(new- old)/ old  * 100)
    else
        p = format("%.2f",(kbs_cZone- kbs_pZone)/ kbs_pZone  * 100)
    end

    p = tonumber(p)

    if(p < 0) then
        zone = "|cffE2252D" .. p;
    elseif(p > 0)then
        zone = "|cff2ed689" .. p;
    else
        zone = "|cFFFFFFFF" .. p;
    end


    if(Felomelo:GetPrevSession(info) == 0) then
        new = kbs_session + 1;
        old = Felomelo:GetPrevSession(info) + 1;
        s = format("%.2f",(new- old)/ old  * 100)
    else
        s = format("%.2f",(kbs_session- Felomelo:GetPrevSession(info))/ Felomelo:GetPrevSession(info)  * 100)
    end

    s = tonumber(s)

    if(s < 0) then
        s = "|cffE2252D" .. s;
    elseif(s > 0)then
        s = "|cff2ed689" .. s;
    else
        s = "|cFFFFFFFF" .. s;
    end

    --update text on tracker
    kbtrackerText:SetText("|cFFFFFFFF          Killing blow tracker\n"
            .. "|cffffcc00Current zone:\n"
            .. "Previous zone:\n\n"
            .. "Current session:\n"
            .. "Previous session:" )
    kbtrackerTextVars:SetText("|cFFFFFFFF"..kbs_cZone .."\n"
            .. kbs_pZone .."\n\n"
            .. kbs_session.."\n"
            .. Felomelo:GetPrevSession(info))
    kbtrackerTextProc:SetText(zone.."%\n\n\n"..s.."%\n")

end

--function for hiding kbtracker frame
function hideKbtracker()
    --IF enableKBT = false then hide otherwise show kbtracker
    if (Felomelo:EnableKBT(info) == false) then
        kbtracker:Hide();
    else
        kbtracker:Show();
    end
end;

--lock or unlock kbtracker based on options
function lockKbtracker()
    if (Felomelo:EnableKBTDrag(info) == true) then
        kbtracker:SetScript("OnDragStart", kbtracker.StartMoving)
        kbtracker:SetScript("OnDragStop", kbtracker.StopMovingOrSizing)
    else
        kbtracker:SetScript("OnDragStart", nil)
        kbtracker:SetScript("OnDragStop", nil)
    end
end

-- function for updating the frame
function updateKbtracker()
    --get zone information
    local pvpType, isFFA, faction = GetZonePVPInfo();

    --increase kbs_cZone
    kbs_cZone = kbs_cZone + 1;

    --increase kbs_session
    kbs_session = kbs_session + 1;

    updateValuesTracker()
end
