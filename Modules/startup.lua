--
-- Created by IntelliJ IDEA.
-- User: Eucalyptisch
-- Date: 26-1-2016
-- Time: 18:58
--

local startUp = CreateFrame("Frame")
startUp:RegisterEvent("PLAYER_LOGIN");

--function that gets called when an event within startUp triggers.
startUp:SetScript("OnEvent", function(self, event, ...)
    if (Felomelo:GetDisableStartupText() ~= true) then
        --print startup message for addon
        print("|cffffff00Felomelo|cff3498db v0.01|cffecf0f1 - type in '/fm' to open the options panel");
    end

    --hide or show minimap icon
    Felomelo:GetEnableMinimapIcon(info)

    --create message and image frames
    createFrames();

    --snap edit killing blow message to screen if enabled
    snapMessage();

    --display or hide the kbtracker
    hideKbtracker();

    --enable or disable drag on startup
    lockKbtracker();

    --set tracker backdrop
    setBackdrop();

    --activate resetting on death
    resetSoundsOnDeath();

    --activate resetting on zone change
    resetSoundsOnZoneChange();

    --shows timer val dropdown
    showSoundsTimerVal();

    --shows ordered list
    showSoundsOrderedList();

    if(Felomelo:GetPrevSession(info) == 0) then
        new = kbs_session + 1;
        old = Felomelo:GetPrevSession(info) + 1;
        p = format("%.2f",(new- old)/ old  * 100)
    else
        p = format("%.2f",(kbs_session- Felomelo:GetPrevSession(info))/ Felomelo:GetPrevSession(info)  * 100)
    end

    p = tonumber(p)

    if(p < 0) then
        zone = "|cffE2252D" .. p;
    elseif(p > 0)then
        zone = "|cff2ed689" .. p;
    else
        zone = "|cFFFFFFFF" .. p;
    end

    kbtrackerText:SetText("|cFFFFFFFF          Killing blow tracker\n"
            .. "|cffffcc00Current zone:\n"
            .. "Previous zone:\n\n"
            .. "Current session:\n"
            .. "Previous session:" )
    kbtrackerTextVars:SetText("|cFFFFFFFF"..kbs_cZone .."\n"
            .. kbs_pZone .."\n\n"
            .. kbs_session.."\n"
            .. Felomelo:GetPrevSession(info))
    kbtrackerTextProc:SetText("|cFFFFFFFF0%\n\n\n"..zone.."%\n")


    setTrackerFont();
end);
