function Felomelo:ToggleEnableKBTDrag(info, value)
    self.db.profile.enableKBTDrag = value;
    lockKbtracker();
end;

function Felomelo:ToggleEnableImageDrag(info, value)
    self.db.profile.enableImageDrag = value;
    lockImageFrame();
end;

function Felomelo:ToggleEnableMessageDrag(info, value)
    self.db.profile.enableMessageDrag = value;
    lockMessageFrame();
end;

function Felomelo:ToggleEnableKBT(info, value)
    self.db.profile.enableKBT = value;
    hideKbtracker();
end;

function Felomelo:ToggleEnableSoundOrderedRestartDeath(info, value)
    self.db.profile.sounds_ordered_restart_death = value;
    resetSoundsOnDeath();
end;

function Felomelo:ToggleEnableSoundOrderedRestartZone(info, value)
    self.db.profile.sounds_ordered_restart_zone = value;
    resetSoundsOnZoneChange();
end;

function Felomelo:ToggleEnableSoundOrderedRestartTime(info, value)
    self.db.profile.sounds_ordered_restart_time = value;
    showSoundsTimerVal();
end;

function Felomelo:ToggleEnableSoundOrderedRestartEnd(info, value)
    self.db.profile.sounds_ordered_restart_end = value;
end;

function Felomelo:SetSoundsOrderedTimeVal(info, value)
    self.db.profile.sounds_ordered_time_val = value;
end;

function Felomelo:SetKillingMessage(info, newValue)
    self.db.profile.killingMessage = newValue;
    setKillingMsg();
end;

function Felomelo:SetMessage(info, newValue)
    self.db.profile.message = newValue;
end;

function Felomelo:ToggleGetEnableStartupText(info, newValue)
    self.db.profile.disableStartupText = newValue;
end;

function Felomelo:SetRed(info, newValue)
    self.db.profile.red = newValue;
end;

function Felomelo:SetBlue(info, newValue)
    self.db.profile.blue = newValue;
end;
function Felomelo:SetGreen(info, newValue)
    self.db.profile.green = newValue;
end;

function Felomelo:SetPrevSession(info, newValue)
    self.db.profile.prevSession = newValue;
end;

function Felomelo:SetFadeInTime(info, value)
    self.db.profile.KBI_fadeInTime = value;
end;

function Felomelo:SetFadeOutTime(info, value)
    self.db.profile.KBI_fadeOutTime = value;
end;

function Felomelo:SetEndDelay(info, value)
    self.db.profile.KBI_duration = value;
end;

function Felomelo:SetModus(info, value)
    self.db.profile.KB_modus = value;
end;

function Felomelo:SetSoundModus(info, value)
    self.db.profile.sounds_modus = value;
end;

function Felomelo:SetMinimapStatus(info, value)
    self.db.profile.minimap.hide = value;
end;

function Felomelo:ToggleEnablePVP(info, value)
    self.db.profile.enablePVP = value;
end;

function Felomelo:ToggleEnableMSG(info, value)
    self.db.profile.enableMSG = value;
end;

function Felomelo:SetChatChannel(info, value)
    self.db.profile.chatChannel = value;
end;

function Felomelo:ToggleEnableKBSound(info, value)
    self.db.profile.enableKBSound = value;
end;

function Felomelo:ToggleEnableKB(info, value)
    self.db.profile.enableKB = value;
end;

function Felomelo:SetVisibleTime(info, value)
    self.db.profile.visibleTime = value;
    setTime()
end;

function Felomelo:SetFadeTime(info, value)
    self.db.profile.fadeTime = value;
    setTime();
end;

function Felomelo:SetTrackerBackground(info, value)
    self.db.profile.trackerBackground = value;
    setBackdrop();
end;

function Felomelo:SetTrackerBorder(info, value)
    self.db.profile.trackerBorder = value;
    setBackdrop();
end;

function Felomelo:SetFontSize(info, value)
    self.db.profile.fontSize = value;
    setFont();
end;

function Felomelo:SetFont(info, value)
    self.db.profile.font = value;
    setFont();
end;

function Felomelo:SetTrackerFont(info, value)
    self.db.profile.trackerFont = value;
    setTrackerFont();
end;

function Felomelo:SetSoundLevel(info, value)
    self.db.profile.soundLevel = value;
end;

function Felomelo:SetOutline(info, value)
    self.db.profile.outline = value;
    setFont();
end;

function Felomelo:ToggleEnableSnap(info, value)
    self.db.profile.enableSnap = value;
    snapMessage();
end;

function Felomelo:ToggleEnableMinimapIcon(info, value)
    self.db.profile.minimap.enableMinimapIcon = value;
end;

function Felomelo:ToggleEnableMSGAnnounce(info, value)
    self.db.profile.enableMSGAnnounce = value;
end;

function Felomelo:ToggleEnableEmotes(info, value)
    self.db.profile.enableEmotes = value;
end;

function Felomelo:SetEmote(info, key)
    if self.db.profile.emotes[key] == nil then
        self.db.profile.emotes[key] = key;
    else
        self.db.profile.emotes[key] = nil;
    end
end;

function Felomelo:SetSound(info, key)
    if self.db.profile.sounds[key] == nil then
        self.db.profile.sounds[key] = key;
    else
        self.db.profile.sounds[key] = nil;
    end
end;

function Felomelo:UncheckAllEmotes()
    self.db.profile.emotes = {};
end;

function Felomelo:UncheckAllSounds()
    self.db.profile.sounds = {};
end;

function Felomelo:SetAnnounceMessage(info, value)
    self.db.profile.messageAnnounce = value;
end;

function Felomelo:SetAnnounceMessageNon(info, value)
    self.db.profile.messageAnnounceNon = value;
end;

function Felomelo:SetChatAnnounceChannel(info, value)
    self.db.profile.chatAnnounceChannel = value;
end;

function Felomelo:SetKBIImage(info, value)
    self.db.profile.KBI_image = value;
    imageObj:SetTexture(value)
end;

function Felomelo:SetKBIWidth(info, value)
    self.db.profile.KBI_width = value;
    resizeKBIImage(value,Felomelo:GetKBIHeight(info))
end;

function Felomelo:SetKBIHeight(info, value)
    self.db.profile.KBI_height = value;
    resizeKBIImage(Felomelo:GetKBIWidth(info),value)
end;

function Felomelo:SetKBMWidth(info, value)
    self.db.profile.KBM_width = value;
    resizeKBMMessage(value,Felomelo:GetKBMHeight(info))
end;

function Felomelo:SetKBMHeight(info, value)
    self.db.profile.KBM_height = value;
    resizeKBMMessage(Felomelo:GetKBMWidth(info),value)
end;

function Felomelo:ToggleKBIDebug(info, value)
    if(value == true) then
        imageAnimationIn:Stop();
        imageAnimationOut:Stop();
        fade()
    end

    self.db.profile.KBI_debug = value;
end;

function Felomelo:SetKBIRelTo(info, value)
     self.db.profile.KBI_relto = value;
end;

 function Felomelo:SetKBIRelPoint(info, value)
     self.db.profile.KBI_relpoint = value;
end;

 function Felomelo:SetKBIXOffset(info, value)
     self.db.profile.KBI_xOffset = value;
end;

function Felomelo:SetKBIYOffset(info, value)
     self.db.profile.KBI_yOffset = value;
end;

 function Felomelo:SetKBIPoint(info, value)
     self.db.profile.KBI_point = value;
end;



function Felomelo:SetKBMRelTo(info, value)
     self.db.profile.KBM_relto = value;
end;

 function Felomelo:SetKBMRelPoint(info, value)
     self.db.profile.KBM_relpoint = value;
end;

 function Felomelo:SetKBMXOffset(info, value)
     self.db.profile.KBM_xOffset = value;
end;

function Felomelo:SetKBMYOffset(info, value)
     self.db.profile.KBM_yOffset = value;
end;

function Felomelo:SetKBMPoint(info, value)
     self.db.profile.KBM_point = value;
end;

function Felomelo:IncrementOrderedIndex()
    self.db.profile.sounds_ordered_cur = Felomelo:GetSoundOrderedIndex() + 1;
end;

function Felomelo:DecrementOrderedIndex()
    if (Felomelo:GetSoundOrderedIndex() ~= 1) then
        self.db.profile.sounds_ordered_cur = Felomelo:GetSoundOrderedIndex() - 1;
        self.db.profile.sounds_ordered_list[Felomelo:GetSoundOrderedIndex()] = nil;
    end
end;

function Felomelo:SetSoundOrderedListItem(info, value, index)
    if index == null then
        self.db.profile.sounds_ordered_list[info[#info]] = value;
    else
        self.db.profile.sounds_ordered_list[index] = value;
    end

end;

function Felomelo:IncrementSoundsOrderedOrder()
    if (options.args.sound.args.sound_ordered.args[tostring(Felomelo:GetSoundsOrderedOrder() + 1)] ~= nil) then
        sounds_ordered_order = Felomelo:GetSoundsOrderedOrder() + 1;
    elseif (Felomelo:EnableSoundOrderedRestartEnd() == true) then
        Felomelo:ResetSoundsOrderedOrder();
    end
end;

function Felomelo:ResetSoundsOrderedOrder()
    sounds_ordered_order = 1;
end;