function Felomelo:GetKillingMessage(info)
    return self.db.profile.killingMessage;
end;

function Felomelo:GetMessage(info)
    return self.db.profile.message;
end;

function Felomelo:GetGreen(info)
    return self.db.profile.green;
end;

function Felomelo:GetRed(info)
    return self.db.profile.red;
end;

function Felomelo:GetBlue(info)
    return self.db.profile.blue;
end;

function Felomelo:GetPrevSession(info)
    return self.db.profile.prevSession;
end;

function Felomelo:EnableKBT(info)
    return self.db.profile.enableKBT;
end;

function Felomelo:EnableSoundOrderedRestartEnd(info)
    return self.db.profile.sounds_ordered_restart_end;
end;

function Felomelo:EnableSoundOrderedRestartDeath(info)
    return self.db.profile.sounds_ordered_restart_death;
end;

function Felomelo:EnableSoundOrderedRestartZone(info)
    return self.db.profile.sounds_ordered_restart_zone;
end;

function Felomelo:EnableSoundOrderedRestartTime(info)
    return self.db.profile.sounds_ordered_restart_time;
end;

function Felomelo:GetSoundLevel(info)
    return self.db.profile.soundLevel;
end;

function Felomelo:GetOutline(info)
    return self.db.profile.outline;
end;

function Felomelo:GetDisableStartupText(info)
    return self.db.profile.disableStartupText;
end;

function Felomelo:EnableSnap(info)
    return self.db.profile.enableSnap;
end;

function Felomelo:EnableKBTDrag(info)
    return self.db.profile.enableKBTDrag;
end;

function Felomelo:EnableMessageDrag(info)
    return self.db.profile.enableMessageDrag;
end;

function Felomelo:EnableImageDrag(info)
    return self.db.profile.enableImageDrag;
end;

function Felomelo:GetTrackerFont(info)
    return self.db.profile.trackerFont;
end;

function Felomelo:GetSound(info)
    return self.db.profile.sound;
end;

function Felomelo:GetFont(info)
    return self.db.profile.font;
end;

function Felomelo:GetTrackerBorder(info)
    return self.db.profile.trackerBorder;
end;

function Felomelo:GetTrackerBackground(info)
    return self.db.profile.trackerBackground;
end;

function Felomelo:GetFontSize(info)
    return self.db.profile.fontSize;
end;

function Felomelo:EnableKBSound(info)
    return self.db.profile.enableKBSound;
end;

function Felomelo:GetVisibleTime(info)
    return self.db.profile.visibleTime;
end;

function Felomelo:GetFadeTime(info)
    return self.db.profile.fadeTime;
end;

function Felomelo:EnableKB(info)
    return self.db.profile.enableKB;
end;

function Felomelo:GetModus(info)
    return self.db.profile.KB_modus;
end;

function Felomelo:GetSoundModus(info)
    return self.db.profile.sounds_modus;
end;

function Felomelo:EnablePVP(info)
    return self.db.profile.enablePVP;
end;

function Felomelo:EnableEmotes(info)
    return self.db.profile.enableEmotes;
end;

function Felomelo:EnableMSG(info)
    return self.db.profile.enableMSG;
end;

function Felomelo:GetChatChannel(info)
    return self.db.profile.chatChannel;
end;

function Felomelo:GetKBIDebug(info)
    return self.db.profile.KBI_debug;
end;

function Felomelo:GetFadeInTime(info)
    return self.db.profile.KBI_fadeInTime;
end;

function Felomelo:GetFadeOutTime(info)
    return self.db.profile.KBI_fadeOutTime;
end;

function Felomelo:GetEndDelay(info)
    return self.db.profile.KBI_duration;
end;

function Felomelo:GetAnnounceMessage(info)
    return self.db.profile.messageAnnounce;
end;

function Felomelo:GetAnnounceMessageNon(info)
    return self.db.profile.messageAnnounceNon;
end;

function Felomelo:GetChatAnnounceChannel(info)
    return self.db.profile.chatAnnounceChannel;
end;

function Felomelo:GetSoundsOrderedTimeVal(info)
    return self.db.profile.sounds_ordered_time_val;
end;

function Felomelo:GetMinimapStatus(info)
    return self.db.profile.minimap.hide;
end;

function Felomelo:GetKBIImage(info)
    return self.db.profile.KBI_image;
end;

function Felomelo:GetKBIWidth(info)
    return self.db.profile.KBI_width;
end;

function Felomelo:GetKBIHeight(info)
    return self.db.profile.KBI_height;
end;

function Felomelo:GetKBMWidth(info)
    return self.db.profile.KBM_width;
end;

function Felomelo:GetKBMHeight(info)
    return self.db.profile.KBM_height;
end;

function Felomelo:EnableMSGAnnounce(info)
    return self.db.profile.enableMSGAnnounce;
end;

function Felomelo:ExistsInEmotes(info, key)
     return self.db.profile.emotes[key] ~= nil;
end;

function Felomelo:ExistsInSounds(info, key)
     return self.db.profile.sounds[key] ~= nil;
end;

function Felomelo:GetKBIPoint(info)
    return self.db.profile.KBI_point;
end;

function Felomelo:GetKBIRelPoint(info)
    return self.db.profile.KBI_relpoint;
end;

function Felomelo:GetKBIRelTo(info)
    return self.db.profile.KBI_relto;
end;

 function Felomelo:GetKBIXOffset(info)
    return self.db.profile.KBI_xOffset;
end;

 function Felomelo:GetKBIYOffset(info)
    return self.db.profile.KBI_yOffset;
end;


function Felomelo:GetKBMPoint(info)
    return self.db.profile.KBM_point;
end;

function Felomelo:GetKBMRelPoint(info)
    return self.db.profile.KBM_relpoint;
end;

function Felomelo:GetKBMRelTo(info)
    return self.db.profile.KBM_relto;
end;

 function Felomelo:GetKBMXOffset(info)
    return self.db.profile.KBM_xOffset;
end;

 function Felomelo:GetKBMYOffset(info)
    return self.db.profile.KBM_yOffset;
end;

function Felomelo:GetSoundOrderedIndex()
    return self.db.profile.sounds_ordered_cur;
end;

function Felomelo:GetSoundOrderedListItem(info, index)
    if index == null then
        return self.db.profile.sounds_ordered_list[info[#info]];
    else
        return self.db.profile.sounds_ordered_list[tostring(index)];
    end
end;

function Felomelo:GetRandomEmote(info)
    length = table.map_length(self.db.profile.emotes)
    if length >= 1 then
        idx = math.random(table.map_length(self.db.profile.emotes));
        count = 0
        val = "";
        for k, v in pairs(self.db.profile.emotes) do
            count = count + 1;
            if count == idx then
                val = v;
             break
            end
        end

        return val;
    else
        return nil;
    end
end;

function Felomelo:GetRandomSound(info)
    length = table.map_length(self.db.profile.sounds)
    if length >= 1 then
        idx = math.random(table.map_length(self.db.profile.sounds));
        count = 0
        val = "";
        for k, v in pairs(self.db.profile.sounds) do
            count = count + 1;
            if count == idx then
                val = v;
             break
            end
        end

        return val;
    else
        return nil;
    end
end;

function Felomelo:GetEnableMinimapIcon(info)
    s = self.db.profile.minimap.enableMinimapIcon
    if(s == true) then
        icon:Show("Felomelo")
    else
        icon:Hide("Felomelo")
    end

    return s;
end;

function Felomelo:GetSoundsOrderedOrder()
    return sounds_ordered_order
end;