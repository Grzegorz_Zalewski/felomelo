## Interface: 70300
## Title: Felomelo
## Notes: An addon for play Felomelo sounds.
## Version: 0.01
## SavedVariables: kbaDB
## OptionalDeps: LibStub, LibSharedMedia-3.0

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\AceAddon-3.0\AceAddon-3.0.lua
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceConfig-3.0\AceConfigRegistry-3.0\AceConfigRegistry-3.0.lua
Libs\AceConfig-3.0\AceConfigDialog-3.0\AceConfigDialog-3.0.lua
Libs\AceConsole-3.0\AceConsole-3.0.lua
Libs\AceConfig-3.0\AceConfigCmd-3.0\AceConfigCmd-3.0.lua
Libs\AceConfig-3.0\AceConfig-3.0.lua
Libs\AceDB-3.0\AceDB-3.0.lua
Libs\AceDBOptions-3.0\AceDBOptions-3.0.lua
Libs\AceEvent-3.0\AceEvent-3.0.lua
Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
Libs\LibDBIcon-1.0\LibDBIcon-1.0.lua
Libs\LibSharedMedia-3.0\LibSharedMedia-3.0.lua
Libs\AceGUI-3.0-SharedMediaWidgets\widget.xml
#@end-no-lib-strip@

core.lua
Modules\Values\default.lua
Modules\Interface\menu.lua
Modules\Values\getters.lua
Modules\Values\setters.lua
Modules\Tracker\functions.lua
Modules\Tracker\frame.lua
Modules\Message\frame.lua
Modules\Message\functions.lua
Modules\Message\event.lua
Modules\startup.lua
