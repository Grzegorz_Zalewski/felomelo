Felomelo = LibStub("AceAddon-3.0"):NewAddon("Felomelo", "AceConsole-3.0", "AceEvent-3.0")


Media = LibStub("LibSharedMedia-3.0")
------------------------------------------------------------------ADD SOUND FILES HERE--------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
--------HOW TO ADD SOUND FILES----------------------------------------------------------------------------------
---1. Make sure that the sound file is either a mp3 file or ogg file. This is important because WoW doesn't support any other soundfiles.
---2. Add the file to the Sounds map. Found at Interface\AddOns\Felomelo\Sounds
---3. Add the following line to this lua file with your own soundname and the path to the sound file: 
---   Media:Register("sound", "HOW YOU WANT THE SOUND FILE TO BE NAMED", [[Interface\AddOns\Felomelo\Sounds\FILENAME.mp3]]);
---4. Save changes to the Felomelo file and  Restart WoW in case it was already open.
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
---Make sure to backup any changes to this file and of the files added to the sounds map when updating this addon. Any changes will be overwritten
---when updating so just a heads up.
----------------------------------------------------------------------------------------------------------------

--Kaelthas SOUNDS
Media:Register("sound", "Kaelthas: Felomelo1", [[Interface\AddOns\Felomelo\Sounds\Kaelthas_Casting_01.ogg]]);
Media:Register("sound", "Kaelthas: Felomelo2", [[Interface\AddOns\Felomelo\Sounds\Kaelthas_Casting_02.ogg]]);
Media:Register("sound", "Kaelthas: Felomelo3", [[Interface\AddOns\Felomelo\Sounds\Kaelthas_Casting_05.ogg]]);


--QUAKE FEMALE SOUNDS
Media:Register("sound", "Quake female: bottomfeeder", [[Interface\AddOns\Felomelo\Sounds\female\bottomfeeder.mp3]]);
Media:Register("sound", "Quake female: dominating", [[Interface\AddOns\Felomelo\Sounds\female\dominating.mp3]]);
Media:Register("sound", "Quake female: firstblood", [[Interface\AddOns\Felomelo\Sounds\female\firstblood.mp3]]);
Media:Register("sound", "Quake female: godlike", [[Interface\AddOns\Felomelo\Sounds\female\godlike.mp3]]);
Media:Register("sound", "Quake female: headshot", [[Interface\AddOns\Felomelo\Sounds\female\headshot.mp3]]);
Media:Register("sound", "Quake female: humiliation", [[Interface\AddOns\Felomelo\Sounds\female\humiliation.mp3]]);
Media:Register("sound", "Quake female: killingspree", [[Interface\AddOns\Felomelo\Sounds\female\killingspree.mp3]]);
Media:Register("sound", "Quake female: monsterkill", [[Interface\AddOns\Felomelo\Sounds\female\monsterkill.mp3]]);
Media:Register("sound", "Quake female: multikill", [[Interface\AddOns\Felomelo\Sounds\female\multikill.mp3]]);
Media:Register("sound", "Quake female: play", [[Interface\AddOns\Felomelo\Sounds\female\prepare.mp3]]);
Media:Register("sound", "Quake female: rampage", [[Interface\AddOns\Felomelo\Sounds\female\rampage.mp3]]);
Media:Register("sound", "Quake female: ultrakill", [[Interface\AddOns\Felomelo\Sounds\female\ultrakill.mp3]]);
Media:Register("sound", "Quake female: unstoppable", [[Interface\AddOns\Felomelo\Sounds\female\unstoppable.mp3]]);
Media:Register("sound", "Quake female: wickedsick", [[Interface\AddOns\Felomelo\Sounds\female\wickedsick.mp3]]);

--QUAKE MALE SOUNDS
Media:Register("sound", "Quake male: wickedsick", [[Interface\AddOns\Felomelo\Sounds\male\wickedsick.mp3]]);
Media:Register("sound", "Quake male: unstoppable", [[Interface\AddOns\Felomelo\Sounds\male\unstoppable.mp3]])
Media:Register("sound", "Quake male: ultrakill", [[Interface\AddOns\Felomelo\Sounds\male\ultrakill.mp3]]);
Media:Register("sound", "Quake male: triplekill", [[Interface\AddOns\Felomelo\Sounds\male\triplekill.mp3]]);
Media:Register("sound", "Quake male: teamkiller", [[Interface\AddOns\Felomelo\Sounds\male\teamkiller.mp3]]);
Media:Register("sound", "Quake male: rampage", [[Interface\AddOns\Felomelo\Sounds\male\rampage.mp3]]);
Media:Register("sound", "Quake male: prepare", [[Interface\AddOns\Felomelo\Sounds\male\prepare.mp3]]);
Media:Register("sound", "Quake male: perfect", [[Interface\AddOns\Felomelo\Sounds\male\perfect.mp3]]);
Media:Register("sound", "Quake male: multikill", [[Interface\AddOns\Felomelo\Sounds\male\multikill.mp3]]);
Media:Register("sound", "Quake male: monsterkill", [[Interface\AddOns\Felomelo\Sounds\male\monsterkill.mp3]]);
Media:Register("sound", "Quake male: megakill", [[Interface\AddOns\Felomelo\Sounds\male\megakill.mp3]]);
Media:Register("sound", "Quake male: ludicrouskill", [[Interface\AddOns\Felomelo\Sounds\male\ludicrouskill.mp3]]);
Media:Register("sound", "Quake male: killingspree", [[Interface\AddOns\Felomelo\Sounds\male\killingspree.mp3]]);
Media:Register("sound", "Quake male: impressive", [[Interface\AddOns\Felomelo\Sounds\male\impressive.mp3]]);
Media:Register("sound", "Quake male: humiliation", [[Interface\AddOns\Felomelo\Sounds\male\humiliation.mp3]]);
Media:Register("sound", "Quake male: holyshit", [[Interface\AddOns\Felomelo\Sounds\male\holyshit.mp3]]);
Media:Register("sound", "Quake male: headshot", [[Interface\AddOns\Felomelo\Sounds\male\headshot.mp3]]);
Media:Register("sound", "Quake male: godlike", [[Interface\AddOns\Felomelo\Sounds\male\godlike.mp3]]);
Media:Register("sound", "Quake male: firstblood", [[Interface\AddOns\Felomelo\Sounds\male\firstblood.mp3]]);
Media:Register("sound", "Quake male: doublekill", [[Interface\AddOns\Felomelo\Sounds\male\doublekill.mp3]]);
Media:Register("sound", "Quake male: dominating", [[Interface\AddOns\Felomelo\Sounds\male\dominating.mp3]]);
Media:Register("sound", "Quake male: combowhore", [[Interface\AddOns\Felomelo\Sounds\male\combowhore.mp3]]);

----------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------END OF SOUND FILE LIST------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------ADD IMAGE FILES HERE--------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------
--------HOW TO ADD IMAGE FILES----------------------------------------------------------------------------------
---1. Make sure that the image file is either a  BLP image or TGA  image. This is important because WoW doesn't support any other image types.
---2. Make sure that the image contains a power of two dimension. Basically this means that both the width and height of an image needs to be one of the following dimensions
--    "8", "16", "32", "64", "128", "256", "512", "1024", "2048". http://wowpedia.org/API_Texture_SetTexture
---3. Add the file to the Images map. Found at Interface\AddOns\Felomelo\Images
---4. Add the following line exactly as the example below with FILENAME being the image name:
---   Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\FILENAME", [[Interface\AddOns\Felomelo\Images\FILENAME.tga]]);
---5. Save changes to the Felomelo file and  Restart WoW in case it was already open.
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
---Make sure to backup any changes to this file and of the files added to the images map when updating this addon. Any changes will be overwritten
---when updating so just a heads up.
----------------------------------------------------------------------------------------------------------------

Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\imageBlue", [[Interface\AddOns\Felomelo\Images\imageBlue.tga]]);
Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\imageFrost", [[Interface\AddOns\Felomelo\Images\imageFrost.tga]]);
Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\imageYellow", [[Interface\AddOns\Felomelo\Images\imageYellow.tga]]);
Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\alliance1", [[Interface\AddOns\Felomelo\Images\alliance1.tga]]);
Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\horde1", [[Interface\AddOns\Felomelo\Images\horde1.tga]]);
Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\alliance2", [[Interface\AddOns\Felomelo\Images\alliance2.tga]]);
Media:Register("images", "Interface\\AddOns\\Felomelo\\Images\\horde2", [[Interface\AddOns\Felomelo\Images\horde2.tga]]);

----------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------END OF IMAGE FILES LIST-----------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

--create minimap icon data
kbaLDB = LibStub("LibDataBroker-1.1"):NewDataObject("Felomelo", {
	type = "data source",
	text = "Felomelo",
	icon = "Interface\\ICONS\\spell_nature_bloodlust",
	OnClick = function() Felomelo:FelomeloSlash() end,
	OnTooltipShow = function(tooltip)
		tooltip:AddLine("|cffecf0f1Felomelo|r\nClick to open");
	end
})

--create Libstub variable for minimap icon and prefix
icon = LibStub("LibDBIcon-1.0")
KBADK = "KBADK-#c58821c0"


--intitialize chatcommand(/kba) and dependencies
function Felomelo:OnInitialize()
	Felomelo:RegisterChatCommand("fm", "FelomeloSlash")
	
	self.db = LibStub("AceDB-3.0"):New("kbaDB", defaults, "Default")	
	options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
	
	LibStub("AceConfig-3.0"):RegisterOptionsTable("Felomelo", options)
	LibStub("AceConfig-3.0"):RegisterOptionsTable("FelomeloProfiles", options.args.profile)
	
	self.FelomeloOptions = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Felomelo", "Felomelo")
	
	icon:Register("Felomelo", kbaLDB, self.db.profile.minimap)
end;


-- open ingame options menu when /kba gets called
function Felomelo:FelomeloSlash(input)
	InterfaceOptionsFrame_OpenToCategory("Felomelo");
	InterfaceOptionsFrame_OpenToCategory("Felomelo");
end;

function interp(s, tab)
	return (s:gsub('($%b{})', function(w)
		return tab[w:sub(3, -2)] or ""
	end))
end

function table.map_length(t)
    local c = 0
    for k,v in pairs(t) do
         c = c+1
    end
    return c
end